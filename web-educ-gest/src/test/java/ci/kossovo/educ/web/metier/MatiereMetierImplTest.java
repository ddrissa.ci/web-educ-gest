package ci.kossovo.educ.web.metier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.educ.web.dao.MatiereRepository;
import ci.kossovo.educ.web.entity.Matiere;
import ci.kossovo.educ.web.exceptions.InvalidEducException;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MatiereMetierImplTest {
	@Autowired
	IMatiereMetier matiereMetier;
	@MockBean
	MatiereRepository matiereRepositoryMock;
	
	
	@Test
	public void creerUnePersone() throws InvalidEducException{
		// given
				Matiere m = new Matiere("Java", "Langages");
				Matiere m1 = new Matiere("Ruby", "Langages");
				m1.setId(1L);
				
				given(matiereRepositoryMock.save(m)).willReturn(m1);
				
				
				// when
				Matiere ms = matiereMetier.creer(m);

				// then
				verify(matiereRepositoryMock).save(m);
				assertThat(ms).isEqualTo(m1);
	}
	

	@Test
	public void creerUneMatierSansLibelle() {
		// given

		Matiere m = new Matiere();
		m.setDescription("langage");
		given(matiereRepositoryMock.save(m)).willThrow(new RuntimeException("Le libelle ne peut etre null"));

		// when
		Matiere m1 = null;
		try {
			m1 = matiereMetier.creer(m);
		} catch (InvalidEducException e) {
			e.printStackTrace();
		}

		// then
		verify(matiereRepositoryMock, never()).save(m);
		assertThat(m1).isEqualTo(null);

	}
	
	
	@Test
	public void modifierMatiere() {
		// given
		Matiere mex = new Matiere("java", "langage");
		mex.setId(3L);
		given(matiereRepositoryMock.findOne(3L)).willReturn(mex);
		
		mex.setLibelle("ruby");
		given(matiereRepositoryMock.save(mex)).willReturn(mex);
		
		//when
		Matiere ms=null;
		try {
			 ms=matiereMetier.modifier(mex);
		} catch (InvalidEducException e) {
			e.printStackTrace();
		}
		
		
		verify(matiereRepositoryMock).save(mex);
		
		assertThat(ms.getLibelle()).isEqualTo("ruby");
		assertThat(ms.getDescription()).isEqualTo(mex.getDescription());
		
	}

}
