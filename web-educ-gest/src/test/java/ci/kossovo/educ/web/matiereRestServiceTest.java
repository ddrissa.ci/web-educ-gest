package ci.kossovo.educ.web;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import ci.kossovo.educ.web.entity.Matiere;
import ci.kossovo.educ.web.metier.IMatiereMetier;
import ci.kossovo.educ.web.services.MatiereRestService;

@RunWith(SpringRunner.class)
@WebMvcTest(MatiereRestService.class)
public class matiereRestServiceTest {
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private IMatiereMetier matiereMetier;
	
	private ObjectMapper mapper;
	
	
	
	// teste findAll()
		@Test
		public void trouverToutesMatieres() throws Exception {
		
		//Donnée	
			Matiere m1=new Matiere("java", "Languages");
			m1.setId(1L);
			Matiere m2=new Matiere("Ruby", "Languages");
			m2.setId(2l);
			
			List<Matiere> matieres= Arrays.asList(m1,m2);
			
			//when
			given(this.matiereMetier.findAll())
			.willReturn(matieres);
			
			//then
			this.mvc.perform(get("/matieres"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.body.length()", is(2)))
			.andExpect(jsonPath("$.body.[0].libelle", is("java")));
			
		}
	

}
