package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.Sale;

public interface SaleRepository extends JpaRepository<Sale, Long> {

}
