package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.TypeEvaluation;

public interface TtpeEvaluationRepository extends JpaRepository<TypeEvaluation, Long> {

}
