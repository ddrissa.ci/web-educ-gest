package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.Filiere;

public interface FiliereRepository extends JpaRepository<Filiere, Long> {

}
