package ci.kossovo.educ.web.metier;

import ci.kossovo.educ.web.entity.Filiere;

public interface IFiliereMetier extends IMetier<Filiere, Long> {

}
