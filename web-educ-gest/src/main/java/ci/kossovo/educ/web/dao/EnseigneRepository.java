package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.Enseigne;
import ci.kossovo.educ.web.entity.EnseigneID;

public interface EnseigneRepository extends JpaRepository<Enseigne, EnseigneID> {

}
