package ci.kossovo.educ.web.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;

@Embeddable
public class Adresse implements Serializable {
	private static final long serialVersionUID = 1L;

	private String quartier;

   /* @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="Contact")
    @Column(name="telephone")
    private List<String> contacts;*/

    
    private String codePostal;

    
    private String email;
    private String mobile;
    private String bureau;
    private String tel;

    public Adresse() {

    }

    public Adresse(String quartier, String codePostal, String email) {
		super();
		this.quartier = quartier;
		this.codePostal = codePostal;
		this.email = email;
	}
    
    

	

	public Adresse(String quartier, String codePostal, String email, String mobile, String bureau, String tel) {
		super();
		this.quartier = quartier;
		this.codePostal = codePostal;
		this.email = email;
		this.mobile = mobile;
		this.bureau = bureau;
		this.tel = tel;
	}

	public String getQuartier() {
        return this.quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }


    public String getCodePostal() {
        return this.codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getBureau() {
		return bureau;
	}

	public void setBureau(String bureau) {
		this.bureau = bureau;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
    
    
    
    
}
