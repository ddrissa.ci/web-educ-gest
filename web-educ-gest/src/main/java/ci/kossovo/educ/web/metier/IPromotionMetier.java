package ci.kossovo.educ.web.metier;

import ci.kossovo.educ.web.entity.Enseigne;
import ci.kossovo.educ.web.entity.EtudiantPomo;
import ci.kossovo.educ.web.entity.Promotion;

public interface IPromotionMetier extends IMetier<Promotion, Long> {
	public EtudiantPomo affecterEtudiant(Long idPromo, Long idEtudiant, int annee );
	public Enseigne affecterEnseigMat(Long idPromo, Long idEnseignant,Long idMatiere);

}
