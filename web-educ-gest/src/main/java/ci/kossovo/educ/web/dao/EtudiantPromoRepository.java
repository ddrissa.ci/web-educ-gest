package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.EtudiantPomo;
import ci.kossovo.educ.web.entity.EtudiantPromoID;

public interface EtudiantPromoRepository extends JpaRepository<EtudiantPomo, EtudiantPromoID> {

}
