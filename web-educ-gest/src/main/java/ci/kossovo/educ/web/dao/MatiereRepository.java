package ci.kossovo.educ.web.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.kossovo.educ.web.entity.Matiere;

public interface MatiereRepository extends JpaRepository<Matiere, Long> {

}
